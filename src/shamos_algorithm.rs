
pub struct Line {
    pub x0: i32,
    pub y0: i32,
    pub x1: i32,
    pub y1: i32,
}

pub fn intersection_algorithm(lines: &[Line]) -> bool {
    let mut intersection = false;
    /*
     * (y1 - y0)(cx - x1) - (cy - y1)(x1 - x0)
     * Formula yields either negative which is left turn,
     * positive which is right turn or null for left turn
     */
    fn orientation(x0: i32, y0: i32, x1: i32, y1: i32, cx: i32, cy: i32) -> i32 {
        return (y1 - y0) * (cx - x1) - (cy - y1) * (x1 - x0);
    }

    /* Intersection */
    for a in 0..lines.len() {
        let next;

        if a == (lines.len() - 1) {
            next = 0;
        } else {
            next = a + 1;
        }

        /* Orientations */
        let o0 = orientation(lines[a].x0, lines[a].y0,
                             lines[a].x1, lines[a].y1,
                             lines[next].x0, lines[next].y0);
        let o1 = orientation(lines[a].x0, lines[a].y0,
                             lines[a].x1, lines[a].y1,
                             lines[next].x1, lines[next].y1);
        let o2 = orientation(lines[next].x0, lines[next].y0,
                             lines[next].x1, lines[next].y1,
                             lines[a].x0, lines[a].y0);
        let o3 = orientation(lines[next].x0, lines[next].y0,
                             lines[next].x1, lines[next].y1,
                             lines[a].x1, lines[a].y1);

        println!("Orientations: {} {} {} {}", o0, o1, o2, o3);

        if o1 > 0 {
            println!("Line {} to {} turns clockwise", a, next);
        } else if o1 < 0 {
            println!("Line {} to {} turns counter clockwise", a, next);
        } else {
            println!("Line {} to {} are collinear", a, next);
        }

        if lines[a].x1 == lines[next].x0 && lines[a].y1 == lines[next].y0 {
            println!("Lines {} and {} connect and extend one another", a, next);
            continue;
        }

        if o0 * o1 > 0 || o2 * o3 > 0 {
            println!("No intersection with {} and {}", a, next);
            continue;
        }

        /* General case */
        if o0 != o1 && o2 != o3 {
            println!("Intersection: general case with {} and {}", a, next);
            intersection = true;
            continue;
        }

        /* Collinearity on all */
        if o0 == 0 && o1 == 0 && o2 == 0 && o3 == 0 {
            println!("Intersection: special case all orientations collinear");
            intersection = true;
            continue;
        }

        println!("Intersection: {} and {}", a, next);
        intersection = true;
    }

    return if intersection { true } else { false };
}
