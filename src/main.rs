
pub mod shamos_algorithm;

fn main() {
    use shamos_algorithm::Line;
    use shamos_algorithm::intersection_algorithm;

    // Closed polygon
    let polygon0: [Line; 5] = [
        Line { x0: 0, y0: 0, x1: 1, y1: 1 },
        Line { x0: 1, y0: 1, x1: 2, y1: 3 },
        Line { x0: 2, y0: 3, x1: 1, y1: 3 },
        Line { x0: 1, y0: 3, x1: 0, y1: 1 },
        Line { x0: 0, y0: 1, x1: 0, y1: 0 },
    ];

    // Polygon with intersection
    let polygon1: [Line; 6] = [
        Line { x0: 0, y0: 0, x1: 2, y1: 1 },
        Line { x0: 2, y0: 1, x1: 4, y1: 1 },
        Line { x0: 4, y0: 1, x1: 5, y1: 2 },
        Line { x0: 5, y0: 2, x1: 3, y1: 4 },
        Line { x0: 4, y0: 4, x1: 0, y1: 3 },
        Line { x0: 0, y0: 3, x1: 0, y1: 0 },
    ];

    // Polygon with an inperfect connection
    let polygon2: [Line; 7] = [
        Line { x0: 0, y0: 0, x1: 3, y1: 1 },
        Line { x0: 3, y0: 1, x1: 4, y1: 2 },
        Line { x0: 4, y0: 2, x1: 5, y1: 3 },
        Line { x0: 5, y0: 3, x1: 6, y1: 4 },
        Line { x0: 5, y0: 4, x1: 4, y1: 5 },
        Line { x0: 4, y0: 5, x1: 1, y1: 4 },
        Line { x0: 1, y0: 4, x1: 0, y1: 0 },
    ];

    let intersection = intersection_algorithm(&polygon0);
    if intersection {
        println!("Polygon 0 contains intersection");
    } else {
        println!("Polygon 0 does not contain intersections");
    }

    println!("");
    let intersection = intersection_algorithm(&polygon1);
    if intersection {
        println!("Polygon 1 contains intersection");
    } else {
        println!("Polygon 1 does not contain intersections");
    }

    println!("");
    let intersection = intersection_algorithm(&polygon2);
    if intersection {
        println!("Polygon 2 contains intersection");
    } else {
        println!("Polygon 2 does not contain intersections");
    }
}

